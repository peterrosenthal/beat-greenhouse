interface MusicParameters {
  checkpoint: string,
  balance: number,
  similarity: number,
  temperature: number,
}

export default MusicParameters;
