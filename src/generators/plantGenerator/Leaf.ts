interface Leaf {
  height: number,
  size: number,
  psi: number,
  theta: number,
}

export default Leaf;
