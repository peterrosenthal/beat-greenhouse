import NoiseSkew from './NoiseSkew';

interface NoiseThreshold {
  value: number,
  skew: NoiseSkew,
}

export default NoiseThreshold;
