import NoiseParameters from './NoiseParameters';

interface AttractionParameters {
  noise: NoiseParameters,
  radius: number,
  kill: number,
}

export default AttractionParameters;
