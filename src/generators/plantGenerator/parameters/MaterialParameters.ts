import MaterialParameter from './MaterialParameter';

interface MaterialParameters {
  branches: MaterialParameter,
  leaves: MaterialParameter,
}

export default MaterialParameters;
