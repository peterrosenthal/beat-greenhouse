interface LeafParameters {
  maxBranchThickness: number,
  size: number,
  theta: number,
  phiAverage: number,
  phiRandomness: number,
  verticalDensity: number,
}

export default LeafParameters;
