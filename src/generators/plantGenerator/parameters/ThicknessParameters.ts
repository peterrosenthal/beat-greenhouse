interface ThicknessParameters {
  fast: number,
  slow: number,
  combination: number,
}

export default ThicknessParameters;
