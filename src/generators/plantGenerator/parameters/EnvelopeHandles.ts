import HandleGroup from './HandleGroup';
import HandleHeights from './HandleHeights';

interface EnvelopeHandles {
  bot: HandleGroup,
  mid: HandleGroup,
  top: HandleGroup,
  heights: HandleHeights,
}

export default EnvelopeHandles;
