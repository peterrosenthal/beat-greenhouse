import ColorParameter from './ColorParameter';

interface MaterialParameter {
  color: ColorParameter,
  roughness: number,
}

export default MaterialParameter;
