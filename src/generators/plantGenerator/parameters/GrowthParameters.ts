import ThicknessParameters from './ThicknessParameters';

interface GrowthParameters {
  iterations: number,
  reach: number,
  thickness: ThicknessParameters,
}

export default GrowthParameters;
