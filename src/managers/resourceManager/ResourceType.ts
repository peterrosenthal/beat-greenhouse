enum ResourceType {
  GLTF_MODEL,
  TEXTURE,
  CUBE_TEXTURE,
  ARRAY_TEXTURES,
}

export default ResourceType;
