import Resource from './Resource';
import ResourceType from './ResourceType';

const resources: Resource[] = [
  {
    name: 'skyboxTexture',
    type: ResourceType.TEXTURE,
    path: 'resources/textures/skybox/skybox.png',
  },
  {
    name: 'workbenchModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/workbench/workbench.gltf',
  },
  {
    name: 'showbenchModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/showbench/showbench.gltf',
  },
  {
    name: 'potModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/pot/pot.gltf',
  },
  {
    name: 'greenhouseModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/greenhouse/greenhouse.gltf',
  },
  {
    name: 'grassTexture',
    type: ResourceType.TEXTURE,
    path: 'resources/textures/grass/grass.jpg',
  },
  {
    name: 'selectionAlpha',
    type: ResourceType.TEXTURE,
    path: 'resources/textures/selection-highlight/alpha.png',
  },
  {
    name: 'genesisMachineModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/genesis-machine/genesis-machine.gltf',
  },
  {
    name: 'combinatorMachineModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/combinator-machine/combinator-machine.gltf',
  },
  {
    name: 'interpreterMachineModel',
    type: ResourceType.GLTF_MODEL,
    path: 'resources/models/interpreter-machine/interpreter-machine.gltf',
  },
];

export default resources;
